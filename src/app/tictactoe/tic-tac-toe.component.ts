import {Component, Output} from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './tic-tac-toe.component.html',
  styleUrls: ['./tic-tac-toe.component.scss']
})

export class TicTacToeComponent {
  title = 'Neon Tic Tac Toe';
  fields: Array<string>;
  currentPlayer: Player;
  playerX: Player;
  playerO: Player;
  gameOverCondition = '';

  constructor() {
    this.fields = Array(9);
    this.playerX = new Player('X');
    this.playerO = new Player('O');
    this.currentPlayer = this.playerX;
  }

  checkField(fieldId: number) {
    if (this.gameOverCondition !== '') {
      return;
    }
    if (!this.fields[fieldId]) {
      this.currentPlayerTakesField(fieldId);
    }
  }

  resetGame() {
    this.fields = Array(9);
    this.playerX.clearFields();
    this.playerO.clearFields();
    this.currentPlayer = this.playerX;
    this.gameOverCondition = '';
  }

  private currentPlayerTakesField(fieldId: number) {
    this.fields[fieldId] = this.currentPlayer.name;
    this.currentPlayer.takeField(fieldId);
    if (this.currentPlayer.ownedFields.length >= 3) {
      this.gameOverCondition = this.checkGameEnd();
    }
    if (!this.gameOverCondition) {
      this.changeActivePlayer();
    }
  }

  private changeActivePlayer() {
    this.currentPlayer = (this.currentPlayer === this.playerX) ?
      this.playerO : this.playerX;
  }

  private checkGameEnd(): string {
    if (this.currentPlayer.checkWinConditions()) {
      return 'Spieler ' + this.currentPlayer.name + ' hat gewonnen.';
    }
    if (Object.values(this.fields).length === this.fields.length) {
      return 'Unentschieden!';
    }
    return '';
  }

}

export class Player {
  name: string;
  ownedFields: Array<number>;

  constructor(name: string, ownedFields: Array<number> = []) {
    this.name = name;
    this.ownedFields = ownedFields;
  }

  checkWinConditions() {
    let winConditions =
      [[0, 1, 2], [3, 4, 5], [6, 7, 8], [0, 3, 6], [1, 4, 7], [2, 5, 8], [0, 4, 8], [2, 4, 6]];
    let hasAWinningBoard = (arr: number[], target: number[]) => target.every(v => arr.includes(v));
    for (let condition of winConditions) {
      if (hasAWinningBoard(this.ownedFields, condition)) {
        return true;
      }
    }
    return false;
  }

  takeField(fieldId: number) {
    this.ownedFields.push(fieldId);
  }

  clearFields() {
    this.ownedFields = [];
  }
}
