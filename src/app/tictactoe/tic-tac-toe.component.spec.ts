import { TestBed } from '@angular/core/testing';
import { TicTacToeComponent } from './tic-tac-toe.component';

describe('AppComponent', () => {
  beforeEach(() => TestBed.configureTestingModule({
    declarations: [TicTacToeComponent]
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(TicTacToeComponent);
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title 'tictactoe'`, () => {
    const fixture = TestBed.createComponent(TicTacToeComponent);
    const app = fixture.componentInstance;
    expect(app.title).toEqual('tictactoe');
  });

  it('should render title', () => {
    const fixture = TestBed.createComponent(TicTacToeComponent);
    fixture.detectChanges();
    const compiled = fixture.nativeElement as HTMLElement;
    expect(compiled.querySelector('.content span')?.textContent).toContain('tictactoe app is running!');
  });
});
