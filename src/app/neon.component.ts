import {Component} from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './neon.component.html',
  styleUrls: ['./neon.component.scss']
})

export class Neon {
  title = 'Neon Browser Games';
  protected readonly TicTacToe: string = 'Tic Tac Toe';
  protected readonly Sudoku: string = 'Sudoku';
}
