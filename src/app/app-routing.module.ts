import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {PageNotFoundComponent} from "./404/404.component";
import {TicTacToeComponent} from "./tictactoe/tic-tac-toe.component";
import {SudokuComponent} from "./sudoku/sudoku.component";

const routes: Routes = [
  {path: 'tictactoe', component: TicTacToeComponent, title: 'Neon Tic Tac Toe'},
  {path: 'sudoku', component: SudokuComponent, title: 'Neon Sudoku'},
  {path: '', redirectTo: '/tictactoe', pathMatch: 'full'},
  {path: '**', component: PageNotFoundComponent, title: 'Neon page not found'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule {
}
