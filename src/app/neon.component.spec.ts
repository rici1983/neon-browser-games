import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { Neon } from './neon.component';

describe('AppComponent', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [RouterTestingModule],
    declarations: [Neon]
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(Neon);
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title 'neon'`, () => {
    const fixture = TestBed.createComponent(Neon);
    const app = fixture.componentInstance;
    expect(app.title).toEqual('neon');
  });

  it('should render title', () => {
    const fixture = TestBed.createComponent(Neon);
    fixture.detectChanges();
    const compiled = fixture.nativeElement as HTMLElement;
    expect(compiled.querySelector('.content span')?.textContent).toContain('neon app is running!');
  });
});
