import {Component} from '@angular/core';
import {BoardComponent} from './board/board.component';
import {SettingClass} from './setting.class';

@Component({
  selector: 'app-root',
  templateUrl: './sudoku.component.html',
  styleUrls: ['./sudoku.component.scss']
})

export class SudokuComponent {
  title: string = 'Neon Sudoku';
  gameOver = '';
  board: BoardComponent;
  solutionFields: Array<number[]> = [];
  visibleFields: Array<number[]> = [];
  playersFields: Array<number[]> = [];
  selectedField!: Array<number>;

  constructor() {
    this.board = new BoardComponent();
    this.solutionFields = BoardComponent.generateBoardLayerWithEmptyFields();
  }

  protected async startNewGame(setting: SettingClass) {
    this.gameOver = '';
    this.selectedField = [];
    this.solutionFields = await this.board.generateSolutionLayer();
    this.createVisibleFields(setting.visibleFields);
    this.playersFields = BoardComponent.generateBoardLayerWithEmptyFields();
  }

  protected selectField(clickEvent: Event) {
    this.selectedField = BoardComponent.getFieldByClickEvent(clickEvent);
    onkeydown = (event) => {
      if (!this.gameOver && this.selectedField.length === 2 && parseInt(event.key) > -1) {
        this.setField(parseInt(event.key));
      }
    }
  }

  protected setField(value: number) {
    this.playersFields[this.selectedField[0]][this.selectedField[1]] = value;
    this.checkForGameEnd();
  }

  protected surrender($event: Event) {
    $event.preventDefault();
    this.playersFields = this.solutionFields;
    this.gameOver = 'failed';
  }

  private createVisibleFields(amount: number) {
    this.visibleFields = BoardComponent.generateBoardLayerWithEmptyFields();
    while (amount > 0) {
      let row = Math.floor(Math.random() * 9);
      let col = Math.floor(Math.random() * 9);
      if (this.visibleFields[row][col] === 0) {
        this.visibleFields[row][col] = this.board.solutionFields[row][col];
        amount--;
      }
    }
  }

  private checkForGameEnd() {
    for (let row in this.solutionFields) {
      for (let col in this.solutionFields[row]) {
        if (
          (this.visibleFields[row][col] !== this.solutionFields[row][col]) &&
          (this.playersFields[row][col] !== this.solutionFields[row][col])
        ) {
          return;
        }
      }
    }
    this.gameOver = 'success';
  }

}
