import {Component, EventEmitter, Input, Output} from '@angular/core';
import {Quadrant} from './quadrant.class';

@Component({
  selector: 'app-board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.scss']
})

export class BoardComponent {
  @Input() solutionFields!: Array<number[]>;
  @Input() visibleFields!: Array<number[]>;
  @Input() playersFields!: Array<number[]>;
  @Output('selectedField') selectedField = new EventEmitter<Event>();
  quadrants: Quadrant[];
  activeCell: Array<number> = [];

  constructor() {
    this.quadrants = [
      new Quadrant(0, 2, 0, 2),
      new Quadrant(0, 2, 3, 5),
      new Quadrant(0, 2, 6, 8),
      new Quadrant(3, 5, 0, 2),
      new Quadrant(3, 5, 3, 5),
      new Quadrant(3, 5, 6, 8),
      new Quadrant(6, 8, 0, 2),
      new Quadrant(6, 8, 3, 5),
      new Quadrant(6, 8, 6, 8)
    ];
  }

  static generateBoardLayerWithEmptyFields(): number[][] {
    let fields = [];
    for (let row = 0; row <= 8; row++) {
      fields[row] = [0, 0, 0, 0, 0, 0, 0, 0, 0];
    }
    return fields;
  }

  static getFieldByClickEvent($event: Event): number[] {
    let field = ($event.target as HTMLBaseElement).closest('span');
    let row = field?.getAttribute('data-row');
    let col = field?.getAttribute('data-col');
    return (row && col) ? [parseInt(row), parseInt(col)] : [];
  }

  async generateSolutionLayer(): Promise<Array<number[]>> {
    this.solutionFields = BoardComponent.generateBoardLayerWithEmptyFields();
    this.generateInitialQuadrants();
    this.fillField(0, 3);
    this.activeCell = [];
    return this.solutionFields;
  }

  protected selectField($event: MouseEvent) {
    this.activeCell = BoardComponent.getFieldByClickEvent($event);
    this.selectedField.emit($event);
  }

  private generateInitialQuadrants() {
    let initialQuadrants = [this.quadrants[0], this.quadrants[4], this.quadrants[8]];
    for (let quadrant of initialQuadrants) {
      let numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9];
      for (let row = quadrant.rowStart; row <= quadrant.rowEnd; row++) {
        for (let col = quadrant.colStart; col <= quadrant.colEnd; col++) {
          let randomIndex = Math.floor(Math.random() * numbers.length);
          this.solutionFields[row][col] = numbers.splice(randomIndex, 1)[0];
        }
      }
    }
  }

  private fillField(row: number, col: number): boolean {
    if (col > 8) {
      col = 0;
      row++;
    }

    if (row > 8) {
      return true;
    }

    if (this.solutionFields[row][col] !== 0) {
      return this.fillField(row, col + 1);
    }

    for (let numberToSet = 1; numberToSet <= 9; numberToSet++) {
      if (this.checkValidity(row, col, numberToSet)) {
        this.solutionFields[row][col] = numberToSet;
        if (this.fillField(row, col + 1)) {
          return true;
        }
        this.solutionFields[row][col] = 0;
      }
    }
    return false;
  }

  private checkValidity(row: number, col: number, numberToSet: number): boolean {
    return (
      this.checkRowValidity(row, numberToSet) &&
      this.checkColValidity(col, numberToSet) &&
      this.checkQuadrantValidity(row, col, numberToSet)
    );
  }

  private checkRowValidity(rowNo: number, numberToSet: number): boolean {
    return this.solutionFields[rowNo].filter(x => x === numberToSet).length < 1;
  }

  private checkColValidity(colNo: number, numberToCheck: number): boolean {
    for (let rowNo = 0; rowNo <= 8; rowNo++) {
      if (this.solutionFields[rowNo][colNo] === numberToCheck) {
        return false;
      }
    }
    return true;
  }

  private checkQuadrantValidity(rowNo: number, colNo: number, numberToSet: number): boolean {
    let currentQuadrant = this.quadrants.find(
      Quadrant => Quadrant.hasField(rowNo, colNo)
    );
    if (!currentQuadrant) return true;

    for (let row = currentQuadrant.rowStart; row <= currentQuadrant.rowEnd; row++) {
      for (let col = currentQuadrant.colStart; col <= currentQuadrant.colEnd; col++) {
        if (this.solutionFields[row][col] === numberToSet) {
          return false;
        }
      }
    }
    return true;
  }
}
