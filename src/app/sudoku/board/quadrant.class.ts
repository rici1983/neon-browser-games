export class Quadrant {
  rowStart: number;
  rowEnd: number;
  colStart: number;
  colEnd: number;

  constructor(rowStart: number, rowEnd: number, colStart: number, colEnd: number) {
    this.rowStart = rowStart;
    this.rowEnd = rowEnd;
    this.colStart = colStart;
    this.colEnd = colEnd;
  }

  hasField(row: number, col: number): boolean {
    return (
      (this.rowStart <= row && this.rowEnd >= row) &&
      (this.colStart <= col && this.colEnd >= col)
    );
  }
}
