import {Component, EventEmitter, Input, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {SettingClass} from "../setting.class";

@Component({
  selector: 'app-difficulty',
  templateUrl: './difficulty.component.html',
  styleUrls: ['./difficulty.component.scss']
})

export class DifficultyComponent {
  @Input() gameOverCondition = false;
  @Output('setting') setting = new EventEmitter<SettingClass>();
  @Output('surrender') surrender = new EventEmitter<Event>();
  gameSettingsForm: FormGroup;
  protected readonly Setting = SettingClass;

  constructor(private fb: FormBuilder) {
    this.gameSettingsForm = this.fb.group({
      'difficulty': [1, [Validators.required]]
    });
  }

  protected submitSettings() {
    this.setting.emit(new SettingClass(this.gameSettingsForm.get('difficulty')?.value));
  }

  protected surrenderGame($event: Event) {
    $event.preventDefault();
    this.surrender.emit($event);
  }

}
