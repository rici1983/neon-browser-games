export class SettingClass {

  static gameSettings = [
    {name: 'Einfach', visibleFields: 40},
    {name: 'Mittel', visibleFields: 35},
    {name: 'Schwer', visibleFields: 30},
    {name: 'Demo', visibleFields: 78}
  ];
  name: string;
  visibleFields: number;

  constructor(settingId: number) {
    this.name = SettingClass.gameSettings[settingId].name;
    this.visibleFields = SettingClass.gameSettings[settingId].visibleFields;
  }

  static getListOfSettings() {
    return SettingClass.gameSettings;
  }
}
