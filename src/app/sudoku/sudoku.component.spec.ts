import { TestBed } from '@angular/core/testing';
import { SudokuComponent } from './sudoku.component';

describe('AppComponent', () => {
  beforeEach(() => TestBed.configureTestingModule({
    declarations: [SudokuComponent]
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(SudokuComponent);
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title 'sudoku'`, () => {
    const fixture = TestBed.createComponent(SudokuComponent);
    const app = fixture.componentInstance;
    expect(app.title).toEqual('sudoku');
  });

  it('should render title', () => {
    const fixture = TestBed.createComponent(SudokuComponent);
    fixture.detectChanges();
    const compiled = fixture.nativeElement as HTMLElement;
    expect(compiled.querySelector('.content span')?.textContent).toContain('sudoku app is running!');
  });
});
