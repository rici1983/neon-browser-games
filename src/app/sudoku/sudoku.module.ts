import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {SudokuComponent} from "./sudoku.component";
import {BoardComponent} from './board/board.component';
import {DifficultyComponent} from './difficulty/difficulty.component';

@NgModule({
  declarations: [
    SudokuComponent,
    BoardComponent,
    DifficultyComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule
  ]
})

export class SudokuModule {
}
