import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {AppRoutingModule} from './app-routing.module';

import {Neon} from "./neon.component";
import {PageNotFoundComponent} from './404/404.component';
import {TicTacToeComponent} from "./tictactoe/tic-tac-toe.component";
import {SudokuModule} from "./sudoku/sudoku.module";

@NgModule({
  declarations: [
    Neon,
    PageNotFoundComponent,
    TicTacToeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    SudokuModule
  ],
  bootstrap: [Neon]
})

export class AppModule {
}
